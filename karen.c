#include <stdio.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>

#define ROOT_UID 0

int main() {
	int uid = geteuid();
	int pid = getpid();

	if (uid != 0) {
		printf("I demand to talk to your kernel!\n");
		printf("Run me as a superuser (sudo).\n");
		return 0;
	}
	else {
		printf("Original priority: %d\n", getpriority(uid, pid));

		int res = setpriority(PRIO_PROCESS, 0, -20);
		printf("Setting new priority to %d\n", getpriority(uid, pid));

		printf("Victory! No one is ahead of me in line!\n");

		//  spawn new shell
		system("/bin/bash");
	}

	return 0;
}
